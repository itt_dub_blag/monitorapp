package ie.ittdublin.monitorapp.activities;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import ie.ittdublin.monitorapp.NetworkManager;
import ie.ittdublin.monitorapp.R;
import ie.ittdublin.monitorapp.fragments.SFragment;
import ie.ittdublin.monitorapp.fragments.BatteryFragment;
import ie.ittdublin.monitorapp.fragments.PSFragment;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private SFragment fragment;

    private Snackbar snackbar;
    private NetworkManager networkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Setup drawer view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open,  R.string.drawer_close);
        NavigationView nvDrawer = (NavigationView) findViewById(R.id.nvView);
        setupDrawerContent(nvDrawer);

        // Load default fragment
        replaceFragment(BatteryFragment.class);

        // Start network manager
        networkManager = new NetworkManager(this);
    }

    @Override
    protected void onPause() {
        // Stop all network operations
        networkManager.cancelActions();
        super.onPause();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.nav_battery_item:
                fragmentClass = BatteryFragment.class;
                break;
            case R.id.nav_ws_item:
                fragmentClass = PSFragment.class;
                break;
            default:
                return;
        }

        replaceFragment(fragmentClass);

        // Highlight the selected item
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.settings_option:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.refresh_option:
                fragment.onRefreshClicked();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public void replaceFragment(Class fragmentClass) {
        try {
            fragment = (SFragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    /**
     * Network Management
     */

    public NetworkManager getNetworkManager() {
        return networkManager;
    }

    public void onNMConnected() {
        if(snackbar != null && snackbar.isShownOrQueued()) snackbar.dismiss();
    }

    public void onNMDisconnected(boolean error) {
        String message = getString(R.string.tcp_disconnected);
        if(error) {
            message = getString(R.string.tcp_connection_error);
        }

        if(snackbar != null && snackbar.isShownOrQueued()) snackbar.dismiss();
        View context = findViewById(android.R.id.content);
        if (context != null) {
            snackbar = Snackbar.make(context, message, Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("Reconnect", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getNetworkManager().connectAndUpdate();
                }
            });
            snackbar.show();
        }
    }

    public void onNMConnecting() {
        updateAndShowSnackbar(getString(R.string.tcp_connecting), Snackbar.LENGTH_INDEFINITE);
    }

    /**
     * Used to display feedback messages to the user
     * @param msg the message to display in the snackbar
     * @param length duration of the snackbar apparition
     */
    private void updateAndShowSnackbar(String msg, int length) {
        if(snackbar != null && snackbar.isShownOrQueued()) snackbar.dismiss();
        View context = findViewById(android.R.id.content);
        if (context != null) {
            snackbar = Snackbar.make(context, msg, length);
            snackbar.show();
        }
    }

    public void alert(int alertStringResId) {
        new AlertDialog.Builder(this)
                .setMessage(getString(alertStringResId))
                .setNeutralButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
