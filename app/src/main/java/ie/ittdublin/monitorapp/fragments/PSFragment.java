package ie.ittdublin.monitorapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ie.ittdublin.monitorapp.R;

/**
 * Display and updates values from an array of pressure sensors.
 */
public class PSFragment extends SFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wi, container, false);
    }

    @Override
    public void onNMResult(String command, String value) {

    }

    @Override
    public void onRefreshClicked() {

    }
}
