package ie.ittdublin.monitorapp.fragments;

import android.app.Fragment;
import android.os.Bundle;

import ie.ittdublin.monitorapp.activities.MainActivity;

/**
 * This abstract class provides methods to be used by all applications components
 * for reacting to network actions.
 */
public abstract class SFragment extends Fragment {
    protected MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mainActivity = (MainActivity) getActivity();
        super.onCreate(savedInstanceState);
    }

    public abstract void onNMResult(String request, String value);

    public abstract void onRefreshClicked();
}
