package ie.ittdublin.monitorapp.fragments;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import ie.ittdublin.monitorapp.R;
import ie.ittdublin.monitorapp.views.HistoryChart;

/**
 * Displays and updates the battery output voltage and an history of those levels for the
 * last 10 minutes.
 */
public class BatteryFragment extends SFragment {

    private TextView voltageView;
    private TextView timestampView;
    private HistoryChart chart;

    // settings
    private int batteryUpdateInterval;
    private int historyUpdateInterval;
    private static final String batteryUpdateCmd = "bl";
    private static final String historyUpdateCmd = "bh";
    private static final String storageUsageCmd = "sf";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_battery, container, false);

        // register various views for later updating
        voltageView = (TextView) view.findViewById(R.id.voltage_tv);
        timestampView = (TextView) view.findViewById(R.id.timestamp_tv);
        chart = (HistoryChart) view.findViewById(R.id.chart);

        return view;
    }

    @Override
    public void onPause() {
        mainActivity.getNetworkManager().cancelActions();
        super.onPause();
    }

    /**
     * Connects and starts updates from the NetworkManager.
     */
    @Override
    public void onResume() {
        chart.setResources();
        mainActivity.getNetworkManager().registerFragment(this);
        // register commands to be performed periodically by the NetworkManager
        loadSettings();
        mainActivity.getNetworkManager().addAction(batteryUpdateCmd, batteryUpdateInterval);
        mainActivity.getNetworkManager().addAction(historyUpdateCmd, historyUpdateInterval);
        // start network actions
        mainActivity.getNetworkManager().connectAndUpdate();

        // request storage usage on the board
        mainActivity.getNetworkManager().request(storageUsageCmd);
        super.onResume();
    }

    /**
     * Updates the correct view when a network request is resolved.
     * @param request The request performed by the NetworkManager.
     * @param res The result yielded.
     */
    @Override
    public void onNMResult(String request, String res) {
        switch(request) {
            case batteryUpdateCmd:
                res = res.split("\n")[0];
                String [] ress = res.split(",");
                String timestamp = ress[0];
                String value = ress[1];
                voltageView.setText(String.format(Locale.ENGLISH, "%sV", value));
                timestampView.setText(timestamp);
                break;
            case historyUpdateCmd:
                chart.addValues(res.split("\n"));
                chart.invalidate();
                break;
            case storageUsageCmd:
                if(res.equals("1\n")) {
                    mainActivity.alert(R.string.storage_full);
                }
                break;
        }
    }

    /**
     * Force the execution of the command `h`
     */
    @Override
    public void onRefreshClicked() {
        mainActivity.getNetworkManager().request(historyUpdateCmd);

    }

    /**
     * Load settings for to this fragment.
     */
    private void loadSettings() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mainActivity);
        Resources res = mainActivity.getResources();

        batteryUpdateInterval = Integer.parseInt(sharedPreferences.getString((
                res.getString(R.string.pref_update_batt_interval)),
                res.getString(R.string.pref_update_batt_interval_df))) * 1000;

        historyUpdateInterval = Integer.parseInt(sharedPreferences.getString(
                res.getString(R.string.pref_update_hist_interval),
                res.getString(R.string.pref_update_hist_interval_df)
        )) * 60000;
    }
}
