package ie.ittdublin.monitorapp;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by alexandre on 12/04/16.
 *
 * The class TCPClient handles establishing a connection to a distant tcp server and requesting data.
 */
public class TCPClient {
    public static final String DEBUG_TAG = "TCPClient.DEBUG";
    public static final String ERROR_TAG = "TCPClient.ERROR";
    private static final int socketTimeout = 5000;

    private PrintWriter out;
    private BufferedReader in;
    private Socket socket;

    /**
     * @param host_addr ip address of the server
     * @param host_port port of the server
     */
    public TCPClient(String host_addr, int host_port) throws IOException {
        InetSocketAddress inetHostAddr = new InetSocketAddress(host_addr, host_port);

        socket = new Socket();
        socket.connect(inetHostAddr, socketTimeout);
        socket.setSoTimeout(socketTimeout);

        // PrintWriter object used to send data to server
        out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream())), true);

        // InputStream object used to read data from the server
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        Log.d(DEBUG_TAG, "Connected");
    }

    /**
     * Sends a request message to the server
     */
    public synchronized void send(String command) {
        if(this.out != null) {
            out.print(command);
            out.flush();
            Log.d(DEBUG_TAG, "Requesting `" + command + "`");
        }
        else Log.d(ERROR_TAG, "Error sending command - not connected");
    }

    /**
     * Disconnects from the server and closes I/O streams
     * @throws IOException
     */
    public void stop() throws IOException {
        out.close();
        in.close();
        socket.close();
    }

    public synchronized String req(String command) throws IOException {
        String result = "";
        String buffer;

        send(command);

        try {
            while(!(buffer = in.readLine()).equals("\0")) {
                result += buffer + "\n";
            }
        } catch (NullPointerException e) {
            throw new IOException("readLine returned null value, server unreachable");
        }

        return result;
    }
}
