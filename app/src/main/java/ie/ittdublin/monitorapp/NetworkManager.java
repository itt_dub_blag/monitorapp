package ie.ittdublin.monitorapp;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import ie.ittdublin.monitorapp.activities.MainActivity;
import ie.ittdublin.monitorapp.fragments.SFragment;

import static java.lang.Integer.parseInt;

/**
 * Created by alexandre on 21/04/16.
 *
 * Handles network configuration, timed updates and notifies
 * the fragment it is bound to for view updates.
 */
public class NetworkManager {
    public static final String DEBUG_TAG = "NetworkManager.DEBUG";
    public static final String ERROR_TAG = "NetworkManager.ERROR";

    public enum state {CONNECTED, CONNECTING, DISCONNECTED}
    private state status = state.DISCONNECTED;

    private MainActivity mainActivity;
    private SFragment registeredFragment;
    private TCPClient tcpClient;
    private Timer timer;
    private ArrayList<Action> actions;

    // settings
    private String hostAddr;
    private int hostPort;
    private static final int autoConnectInterval = 5000;

    public NetworkManager(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void registerFragment(SFragment fragment) {
        registeredFragment = fragment;
        actions = new ArrayList<>();
    }

    /**
     * Loads settings from the android shared preferences.
     */
    public void loadSettings() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mainActivity);
        Resources res = mainActivity.getResources();

        hostAddr = sharedPreferences.getString(
                res.getString(R.string.PREF_HOST_ADDR),
                res.getString(R.string.PREF_HOST_ADDR_DF));

        hostPort = parseInt(sharedPreferences.getString(
                res.getString(R.string.PREF_HOST_PORT),
                res.getString(R.string.PREF_HOST_PORT_DF)));
    }

    /**
     * Adds an action to the network manager.
     * Actions contain information (command and interval) for updates that will be performed when
     * the network manager is connected to the server.
     * @param cmd request command to send to the server
     * @param updateInterval interval between requests
     */
    public void addAction(String cmd, int updateInterval) {
        // add action to action list
        actions.add(new Action(cmd, updateInterval));
    }

    /**
     * Tries to connect to the server and launch updates.
     * If the connection fails the network manager will retry to connect periodically.
     * If the connection succeeds, the currently registered actions will be performed.
     */
    public void connectAndUpdate() {
        loadSettings();
        new ConnectionAsyncTask().execute();

        setStatus(state.CONNECTING);
    }

    /**
     * Allows requests to be performed manually
     * @param command the command to send to the server.
     */
    public void request(String command) {
        new UpdateAsyncTask().execute(command);
    }

    /**
     * Starts all currently registered actions.
     * For all actions, a timertask is created and cued in a timer.
     */
    public void startUpdateActions() {
        // clean all timed tasks just in case.
        if (timer != null) timer.cancel();
        // schedule update tasks
        final Handler taskHandler = new Handler();
        TimerTask updateTask = null;
        timer = new Timer();
        for(final Action a : actions) {
            updateTask = new TimerTask() {
                @Override
                public void run() {
                    taskHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            new UpdateAsyncTask().execute(a.command);
                        }
                    });
                }
            };
            timer.schedule(updateTask, 0, a.updateInterval);
        }
    }

    /**
     * Cancels every tasks currently cued and closes socket if connected.
     */
    public void cancelActions() {
        if (timer != null) {
            timer.cancel();
            timer = null; // hack to determine if a timer is running (timer == null if not running)

            Log.d(DEBUG_TAG, "Timer and tasks cancelled");
        }

        if (tcpClient != null) {
            try {
                tcpClient.stop();
                Log.d(DEBUG_TAG, "TCP Client stopped");
            } catch (IOException e) {
                e.printStackTrace();
            }
            tcpClient = null;
        }

        setStatus(state.DISCONNECTED);
    }

    private void setStatus(state state) {
        status = state;
        switch(status) {
            case CONNECTED:
                mainActivity.onNMConnected();
                break;
            case CONNECTING:
                mainActivity.onNMConnecting();
                break;
            case DISCONNECTED:
                mainActivity.onNMDisconnected(false);
                break;
        }
    }

    private class ConnectionAsyncTask extends AsyncTask<Void, Void, Boolean> {

        /**
         * Tries to create a new TCPClient with current settings.
         * @return True if the client has been created successfully.
         */
        @Override
        protected Boolean doInBackground(Void... params) {
            if (tcpClient != null) return true;

            // Try to create a new client
            try {
                Log.d(DEBUG_TAG, "Attempting connection to " + hostAddr + ":" + hostPort);
                tcpClient = new TCPClient(hostAddr, hostPort);
                return true;
            } catch(IOException e) {
                Log.d(ERROR_TAG, mainActivity.getString(R.string.tcp_connection_error));
                return false;
            }
        }

        /**
         * Handles the status of the network manager. Starts actions if connected.
         * Otherwise starts auto connecting every `autoConnectInterval` ms.
         * @param connected True if a TCPClient has been created.
         */
        @Override
        protected void onPostExecute(Boolean connected) {
            if (connected) {
                setStatus(state.CONNECTED);
                // stop timers if autoConnect
                if (timer != null) timer.cancel();
                // start battery updates
                startUpdateActions();
                return;
            }

            // If connection failed
            if (timer != null) return; // FIXME: check if timer is running
            timer = new Timer();
            final Handler taskHandler = new Handler();

            TimerTask connectTask = new TimerTask() {
                @Override
                public void run() {
                    taskHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            new ConnectionAsyncTask().execute();
                        }
                    });
                }
            };
            timer.schedule(connectTask, autoConnectInterval, autoConnectInterval);
        }
    }

    /**
     * This class performs an action that require a single value from the server.
     */
    private class UpdateAsyncTask extends AsyncTask<String, Void, String> {
        private String command;

        @Override
        protected String doInBackground(String... params) {
            try {
                if (tcpClient == null) return null;
                this.command = params[0];
                return tcpClient.req(this.command);
            } catch(IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Updates the view with server data. If an error occurred, stops timed updates.
         * @param result data retrieved from the server
         */
        @Override
        protected void onPostExecute(String result) {
            if (result == null) {
                cancelActions();
                mainActivity.onNMDisconnected(true);
                return;
            }
            Log.d(DEBUG_TAG, "new reading: " + result);
            registeredFragment.onNMResult(this.command, result);
        }
    }

    /**
     * Class holding an action information.
     */
    private class Action {
        public String command;
        public int updateInterval;

        public Action(String command, int updateInterval) {
            this.command = command;
            this.updateInterval = updateInterval;
        }
    }
}
