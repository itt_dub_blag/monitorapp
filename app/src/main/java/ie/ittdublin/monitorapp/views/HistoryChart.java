package ie.ittdublin.monitorapp.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ie.ittdublin.monitorapp.R;

/**
 * Created by alexandre on 28/04/16.
 *
 * Contains data from fetchHistory request.
 * Renders data as a LineChart on screen.
 */
public class HistoryChart extends LineChart {
    private ArrayList<Entry> entries = new ArrayList<>();
    private ArrayList<String> xVals = new ArrayList<>();

    public HistoryChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public HistoryChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Sets application context for future callbacks and applies styling options
     */
    public void setResources() {
        // styling options
        getLegend().setEnabled(false);
        getAxisLeft().setValueFormatter(new YValueFormatter());
        getAxisRight().setEnabled(false);
        setScaleEnabled(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MMarkerView mv = new MMarkerView(getContext(), R.layout.custom_marker_view);

        // set the marker to the chart
        setMarkerView(mv);
    }

    /**
     * Creates a new dataSet and applies it to the LineChart with styling options.
     * @param values values to add
     */
    public void addValues(String[] values) {

        entries.clear();
        xVals.clear();

        float maxValue = 0f;
        float currentValue;

        for(int i = 0; i < values.length; i++) {
            String[] s = values[i].split(",");
            if(s.length < 2) return;
            currentValue = Float.parseFloat(s[1]);
            if (currentValue > maxValue) maxValue = currentValue;
            entries.add(new Entry(currentValue, i));
            xVals.add(s[0].substring(11, s[0].length()));
        }

        LineDataSet dataSet = new LineDataSet(entries, "");
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);
        LineData lineData = new LineData(xVals, dataSets);

        // styling options
        int fillColor = ContextCompat.getColor(getContext(), R.color.red);
        int textColor = ContextCompat.getColor(getContext(), R.color.black);

        dataSet.setColor(fillColor);
        dataSet.setValueTextColor(textColor);
        dataSet.setValueTextSize(10f);
        dataSet.setDrawCircles(false);
        dataSet.setCircleColor(textColor);
        dataSet.setLineWidth(2f);
        dataSet.setFillColor(fillColor);
        dataSet.setFillAlpha(220);
        dataSet.setDrawFilled(true);
        dataSet.setHighLightColor(textColor);
        dataSet.setDrawValues(false);

        getAxisLeft().setAxisMinValue(0);
        getAxisLeft().setAxisMaxValue(maxValue + 1f);

        setData(lineData);
        setDescription("last update: " + new Date().toString());
    }

    /**
     * Formats xValues to the correct format in minutes and seconds (0:00)
     * @param xVal original xValue
     * @param duration duration of the history in seconds
     * @param size total number of values
     * @return formatted string of the xValue in minutes and seconds
     */
    private String getMinutesFromXVal(int xVal, int duration, float size) {
        float c = xVal/size;
        int totalSecs = Math.round(c * duration);
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;
        return String.format(Locale.ENGLISH, "%2d:%02d", minutes, seconds);
    }

    /**
     * Adds correct unit (Volt) to the Y values.
     */
    private class YValueFormatter implements YAxisValueFormatter {

        @Override
        public String getFormattedValue(float value, YAxis yAxis) {
            return Float.valueOf(value).intValue() + "V";
        }
    }
}
