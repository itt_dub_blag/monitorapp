package ie.ittdublin.monitorapp.views;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import java.util.Locale;

import ie.ittdublin.monitorapp.R;

public class MMarkerView extends MarkerView {

    private TextView tvContent;

    public MMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    // callbacks everytime the marker-view is redrawn.
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        tvContent.setText(String.format(Locale.ENGLISH, "%.3fV", e.getVal()));
    }

    @Override
    public int getXOffset(float xpos) {
        // center the marker-view horizontally
        return -(getWidth() / 2);
    }

    @Override
    public int getYOffset(float ypos) {
        // place the marker-view above the selected value
        return -getHeight() - 20;
    }
}

